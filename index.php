<?php
function bouncingBall($h, $bounce, $window) {
    if ($h <= 0) return -1;
    if (0 >= $bounce || $bounce >= 1) return -1;
    if ($h <= $window) return -1;
    $result = 0;
    while($h > $window) {
        ++$result;
        $h *= $bounce;
        if ($h > $window) ++$result;
    }
    if ($result === 0) return 1;
    return $result;
}

$h = rand(1,100);
$bounce = rand(1,100)/100;
$window = rand(1,10);
echo bouncingBall($h, $bounce, $window);